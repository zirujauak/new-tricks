# new-tricks

An old dog learns rust while working through old 
[Advent Of Code](https://adventofcode.com) exercises.

This is not going to be pretty.

General usage:

**$ new-tricks -f [input-filename] year day input**

where:
*  `input-filename` is an optional filename to read input from
*  `year` is 2015, 2016, 2017, or 2018 (or will be when those challenges are 
added)  
*  `day` is 1 through 25 (again, dependent on progress)  
*  `input` is an optional string to use as input  

If an `input` string is specified, the `-f` option is ignored.  If `-f` is not 
used and there is no `input`, a default filename of `input-{year}-{day}.txt` 
is used.
