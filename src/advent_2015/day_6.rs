extern crate regex;

use regex::Regex;

pub fn execute(input: &str) {
    let instructions = parse_input(&input);
    let mut toggle_grid: Vec<Vec<bool>> = Vec::with_capacity(999);
    let mut brightness_grid: Vec<Vec<u32>> = Vec::with_capacity(999);

    // Initialize the toggle and brightness arrays
    for _ in 0..1000 {
        let mut v: Vec<bool> = Vec::with_capacity(999);
        let mut b: Vec<u32> = Vec::with_capacity(999);
        for _ in 0..1000 {
            v.push(false);
            b.push(0);
        }
        toggle_grid.push(v);
        brightness_grid.push(b);
    }

    for instruction in instructions.iter() {
        let (sx,sy) = instruction.1;
        let (tx,ty) = instruction.2;
        for i in sx..tx+1 {
            for j in sy..ty+1 {
                if instruction.0 == "turn on" {
                    toggle_grid[i][j] = true;
                    brightness_grid[i][j] = brightness_grid[i][j] + 1;
                } else if instruction.0 == "turn off" {
                    toggle_grid[i][j] = false;
                    if brightness_grid[i][j] > 0 {
                        brightness_grid[i][j] = brightness_grid[i][j] - 1;
                    }
                } else if instruction.0 == "toggle" {
                    toggle_grid[i][j] = !toggle_grid[i][j];
                    brightness_grid[i][j] = brightness_grid[i][j] + 2;
                }
            }
        }
    }

    let mut toggle_count:u32 = 0;
    let mut brightness:u32 = 0;
    for i in 0..1000 {
        for j in 0..1000 {
            if toggle_grid[i][j] {
                toggle_count = toggle_count + 1;
            }
            brightness = brightness + brightness_grid[i][j];
        }
    }

    println!("There are {} lights turned on!", toggle_count);
    println!("The total brightness is {}", brightness);
}

fn parse_input(input: &str) -> Vec<(String, (usize,usize), (usize,usize))> {
    let mut v:Vec<(String,(usize,usize),(usize,usize))> = Vec::new();

    let re = Regex::new(r"([[:alpha:] ]+) (\d+),(\d+) through (\d+),(\d+)").unwrap();
    for line in input.lines() {
        let cap = re.captures(line).unwrap();
        v.push((String::from(cap.get(1).unwrap().as_str()), 
                (cap.get(2).unwrap().as_str().parse::<usize>().unwrap(), cap.get(3).unwrap().as_str().parse::<usize>().unwrap()),
                (cap.get(4).unwrap().as_str().parse::<usize>().unwrap(), cap.get(5).unwrap().as_str().parse::<usize>().unwrap())));
    }

    return v;
}
