fn parse_input(input:&str) -> Vec<Vec<u32>> {
    let mut output: Vec<Vec<u32>> = Vec::new();

    for line in input.lines() {
        let dimensions:Vec<&str> = line.split('x').collect();
        // Convert to u32
        let mut v = vec![dimensions[0].parse::<u32>().unwrap(), 
                         dimensions[1].parse::<u32>().unwrap(), 
                         dimensions[2].parse::<u32>().unwrap()];
        // Sort
        v.sort();
        output.push(v);
    }

    return output;
}

pub fn execute(input: &str) {
    let packages = parse_input(&input);
    let mut area:u32 = 0;
    let mut ribbon:u32 = 0;
    for package in packages.iter() {
        let r = wrap_package(package.to_vec());
        area = area + r.0;
        ribbon = ribbon + r.1;
    }

    println!("Total paper area: {} sq ft", area);
    println!("Total ribbon: {} ft", ribbon);
}

fn wrap_package(v: Vec<u32>) -> (u32, u32) {
    // Package dimensions are already sorted, slack is area of smallest side
    let slack = v[0] * v[1];

    // 2lw + 2wh + 2hl + slack
    let area = (2 * v[0] * v[1]) +
                (2 * v[0] * v[2]) +
                (2 * v[1] * v[2]) +
                slack;

    // Area of smallest side + volume
    let ribbon = (2 * v[0]) + (2 * v[1]);
    let volume = v[0] * v[1] * v[2];

    return (area, ribbon + volume);
}
