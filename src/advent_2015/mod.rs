mod day_1;
mod day_2;
mod day_3;
mod day_4;
mod day_5;
mod day_6;
mod day_7;
mod day_8;

pub fn dispatch(day: &str, input: &str) {
    match day {
        "1" => day_1::execute(input),
        "2" => day_2::execute(input),
        "3" => day_3::execute(input),
        "4" => day_4::execute(input),
        "5" => day_5::execute(input),
        "6" => day_6::execute(input),
        "7" => day_7::execute(input),
        "8" => day_8::execute(input),
        _ => println!("Unrecognized day: {}", day)
    }
}