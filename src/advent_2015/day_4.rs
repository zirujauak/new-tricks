extern crate md5;

pub fn execute(input: &str) {
    let base = &input.trim();
    let mut n:u32 = 0;
    let mut n_5:u32 = 0;
    let mut d_5:String;
    let mut n_6:u32 = 0;
    let mut d_6:String;

    while n_5 == 0 || n_6 == 0 {
        let digest = hash(base, n);
        if n_5 == 0 && digest.clone()[0..5] == *"00000" {
            n_5 = n;
            d_5 = digest.clone().to_string();
            println!("{} yields {}", n_5, d_5);
        }
        if n_6 == 0 && digest.clone()[0..6] == *"000000" {
            n_6 = n;
            d_6 = digest.clone().to_string();
            println!("{} yields {}", n_6, d_6);
        }
        n = n + 1;
    }
}

fn hash(base:&str, n:u32) -> String {
    let mut str = String::from(base);
    str.push_str(&n.to_string());
    let digest = md5::compute(str.as_bytes());
    return format!("{:x}", digest);
}
