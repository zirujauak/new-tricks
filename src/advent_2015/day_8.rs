extern crate regex;

use regex::Regex;

pub fn execute(input: &str) {
    let mut code_len:usize = 0;
    let mut str_len:usize = 0;
    let mut encoded_len:usize = 0;

    let re = Regex::new(r"\\x[a-f0-9]{2}").unwrap();

    for line in input.lines() {
        code_len = code_len + line.trim().len();
        let l1 = re.replace_all(line.trim(), "X");  // \x## sequences -> X
        let l2 = l1.replace("\\\"", "\"");          // \" -> "
        let l3 = l2.replace("\\\\", "\\");          // \\\\ -> \
        str_len = str_len + l3.len() - 2;           // Subtract 2 for leading and trailing "
        let l6 = line.trim().replace("\\", "\\\\"); // \\ -> \\\\
        let l7 = l6.replace("\"", "\\\"");          // " -> \""
        encoded_len = encoded_len + l7.len() + 2;   // Add 2 for leading and trailing "
    }

    println!("The length of the coded string literals is {}", code_len);
    println!("The length of the strings in memory is {}", str_len);
    println!("The difference is {}", code_len - str_len);
    println!("The length of encoded code string literals is {}", encoded_len);
    println!("The difference is {}", encoded_len - code_len);
}
