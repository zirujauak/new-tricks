use std::collections::HashMap;

pub fn execute(input: &str) {
    let mut deliveries = deliver_presents(&input.trim());

    let mut house_count:u32 = 1;
    for (_, _) in deliveries.iter() {
        house_count = house_count + 1;
    }

    println!("Houses delivered to: {}", house_count);

    deliveries = robo_deliver(&input.trim());
    house_count = 0;

    for (_, _) in deliveries.iter() {
        house_count = house_count + 1;
    }

    println!("Houses robo-delivered to: {}", house_count);
}

fn deliver_present(mut r:HashMap<(i32,i32),u32>, x:i32, y:i32) -> HashMap<(i32,i32),u32> {
    let k = (x,y);
    r.insert(k, match r.get(&k) {
            Some(f) => f + 1,
            None => 1});
    return r;
}

fn deliver_presents(route: &str) -> HashMap<(i32,i32),u32> {
    let mut r = HashMap::new();
    let mut x = 0;
    let mut y = 0;

    for c in route.chars() {
        match c {
            '<' => x = x - 1,
            '>' => x = x + 1,
            'v' => y = y - 1,
            '^' => y = y + 1,
            _ => panic!("Unexpected route character {}", c)
        }

        r = deliver_present(r, x, y);
    }

    return r;
}

fn robo_deliver(route: &str) -> HashMap<(i32,i32),u32> {
    let mut r = HashMap::new();
    let mut santa_x = 0;
    let mut santa_y = 0;
    let mut robot_x = 0;
    let mut robot_y = 0;

    let mut i = route.chars();
    loop {
        let santa = match i.next() {
            Some(f) => f,
            None => return r
        };

        match santa {
            '<' => santa_x = santa_x - 1,
            '>' => santa_x = santa_x + 1,
            'v' => santa_y = santa_y - 1,
            '^' => santa_y = santa_y + 1,
            _ => panic!("Unexpected route character {}", santa)
        }

        r = deliver_present(r, santa_x, santa_y);

        let robot = match i.next() {
            Some(f) => f,
            None => return r
        };

        match robot {
            '<' => robot_x = robot_x - 1,
            '>' => robot_x = robot_x + 1,
            'v' => robot_y = robot_y - 1,
            '^' => robot_y = robot_y + 1,
            _ => panic!("Unexpected route character {}", robot)
        }

        r = deliver_present(r, robot_x, robot_y);
    }
}