extern crate fancy_regex;

use fancy_regex::*;

pub fn execute(input: &str) {
    let mut nice_strings = count_nice_strings(&input);
    println!("Nice strings: {}", nice_strings);
    nice_strings = count_nice_strings_two(&input);
    println!("Nice strings, two: {}", nice_strings);
}

fn count_nice_strings(input: &str) -> u32 {
    let mut nice_count: u32 = 0;
    let vowel_re = Regex::new(r".*[aeiou].*[aeiou].*[aeiou].*").unwrap();
    let naughty_re = Regex::new(r"(ab|cd|pq|xy)").unwrap();
    let double_re = Regex::new(r"(.)\1").unwrap();

    for line in input.lines() {
        if vowel_re.is_match(line).unwrap() && double_re.is_match(line).unwrap() && !naughty_re.is_match(line).unwrap() {
            nice_count = nice_count + 1;
        }
    }

    return nice_count;
}

fn count_nice_strings_two(input: &str) -> u32 {
    let mut nice_count: u32 = 0;
    let repeat_re = Regex::new(r"(..).*\1").unwrap();
    let separated_re = Regex::new(r"(.)[^\1]\1").unwrap();

    for line in input.lines() {
        if repeat_re.is_match(line).unwrap() && separated_re.is_match(line).unwrap() {
            nice_count = nice_count + 1;
        }
    }

    return nice_count;
}
