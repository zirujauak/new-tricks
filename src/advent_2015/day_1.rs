pub fn execute(input: &str) {
    let floors = find_floors(&input);

    println!("Final floor: {}", floors.0);
    println!("Entered the basement at position: {}", floors.1);
}

fn find_floors(input: &str) -> (i32, i32) {
    let mut floor: i32 = 0;
    let mut basement_position: i32 = -1;
    for (pos, c) in input.chars().enumerate() {
        match c {
            '(' => floor = floor + 1,
            ')' => floor = floor - 1,
            _ => continue,
        }
        
        if basement_position <0 && floor < 0 {
            basement_position = (pos as i32) + 1;
        }
    }

    (floor, basement_position)
}

#[test]
fn floor_test() {
    assert_eq!(find_floors("()"), (0, -1));
    assert_eq!(find_floors(")(("), (1, 1));
}
