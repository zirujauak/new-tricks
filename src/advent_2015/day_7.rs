use std::collections::*;

#[derive(Debug)]
#[derive(PartialEq)]
#[derive(Eq)]
#[derive(Hash)]
#[derive(Clone)]
enum Source {
    Constant(u32),
    Wire(String),
}

#[derive(Debug)]
enum Gate {
    NOP(Source),
    AND(Source, Source),
    OR(Source, Source),
    NOT(Source),
    LSHIFT(Source, Source),
    RSHIFT(Source, Source),
}

#[derive(Debug)]
struct Instruction {
    gate: Gate,
    output: Source,    
}

fn parse_source(s: &str) -> Source {
    return match s.parse::<u32>() {
        Ok(n) => Source::Constant(n),
        Err(_) => Source::Wire(String::from(s)),
    };
}

fn parse_input(input: &str) -> Vec<Instruction>{
    let mut v:Vec<Instruction> = Vec::new();

    for line in input.lines() {
        let parts:Vec<&str> = line.split(' ').collect();
        let mut i = 0;

        let source = parts[i];
        if source != "NOT"  {
            i = i + 1;
        }

        let gate = match parts[i] {
            "AND" => { i = i + 2;
                       Gate::AND(parse_source(source), parse_source(&parts[i-1]))},
            "OR" => { i = i + 2;
                      Gate::OR(parse_source(source), parse_source(&parts[i-1]))},
            "LSHIFT" => { i = i + 2;
                          Gate::LSHIFT(parse_source(source), parse_source(&parts[i-1]))},
            "RSHIFT" => { i = i + 2;
                          Gate::RSHIFT(parse_source(source), parse_source(&parts[i-1]))},
            "NOT" => { i = i + 2;
                       Gate::NOT(parse_source(&parts[i-1])) },
            "->" => Gate::NOP(parse_source(source)),
            _ => panic!("Unknown instruction in '{}': {}", line, parts[i]),
        };
        i = i + 1;
        let instruction = Instruction {
            gate: gate,
            output: parse_source(&parts[i])
        };

        v.push(instruction);
    }

    return v;
}

fn source_ready(s: &Source, v: &HashMap<String, u32>) -> bool {
    return match s {
        Source::Constant(_) => true,
        Source::Wire(w) => v.contains_key(w),
    };
}

fn source_value(s: &Source, ref v: &HashMap<String, u32>) -> u32 {
    return match s {
        Source::Constant(c) => *c,
        Source::Wire(w) => *(v.get(w).unwrap()),
    };
}

pub fn execute(input: &str) {
    let mut instructions = parse_input(&input);

    println!("Building circuit...");
    let values = execute_instructions(&instructions);
    let a = values.get("a").unwrap();
    println!("The value of wire a is {}", a);

    let n = instructions.len() as u32;
    for i in 0..n {
        if match &instructions[i as usize].output {
            Source::Wire(w) => w,
            _ => "",
        } == "b" {
            instructions[i as usize] = Instruction { gate: Gate::NOP(Source::Constant(*a)),
                                            output: Source::Wire(String::from("b"))};
        }
    }

    println!("Rewiring 'b' with signal {}", a);
    println!("Building circuit...");
    let values = execute_instructions(&instructions);
    println!("The value of wire a is {}", values.get("a").unwrap());
}

fn execute_instructions(ref instructions: &Vec<Instruction>) -> HashMap<String, u32> {
    let mut unresolved:HashSet<String> = HashSet::default();
    let mut values:HashMap<String, u32> = HashMap::new();

    for inst in *instructions {
        unresolved.insert(match &inst.output {
            Source::Wire(w) => String::from(w),
            Source::Constant(_) => panic!("Output to wire: {:?}", inst),
        });
    }

    while !unresolved.is_empty() {
        for inst in *instructions {
            match &inst.output  {
                Source::Constant(_) => panic!("Output to wire: {:?}", inst),
                Source::Wire(w) => {
                    match &inst.gate {
                        Gate::AND(s1, s2) => {
                            if source_ready(s1, &values) && source_ready(s2, &values) {
                                let v = source_value(s1, &values) & source_value(s2, &values);
                                values.insert(String::from(w), v);
                                unresolved.remove(w);
                            }
                        },
                        Gate::OR(s1, s2) => {
                            if source_ready(s1, &values) && source_ready(s2, &values) {
                                let v = source_value(s1, &values) | source_value(s2, &values);
                                values.insert(String::from(w), v);
                                unresolved.remove(w);
                            }
                        },
                        Gate::LSHIFT(s1, s2) => {
                            if source_ready(s1, &values) && source_ready(s2, &values) {
                                let v = source_value(s1, &values) << source_value(s2, &values);
                                values.insert(String::from(w), v);
                                unresolved.remove(w);
                            }
                        },
                        Gate::RSHIFT(s1, s2) => {
                            if source_ready(s1, &values) && source_ready(s2, &values) {
                                let v = source_value(s1, &values) >> source_value(s2, &values);
                                values.insert(String::from(w), v);
                                unresolved.remove(w);
                            }
                        },
                        Gate::NOT(s1) => {
                            if source_ready(s1, &values) {
                                let v = !source_value(s1, &values);
                                values.insert(String::from(w), v);
                                unresolved.remove(w);
                            }
                        },
                        Gate::NOP(s1) => {
                            if source_ready(s1, &values) {
                                let v = source_value(s1, &values);
                                values.insert(String::from(w), v);
                                unresolved.remove(w);
                            }
                        },
                    }
                }  
            }
        }
    }

    return values;
}
