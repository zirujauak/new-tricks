extern crate getopts;

use getopts::Options;
use std::env;
use std::fs;

mod advent_2015;

fn print_usage(program: &str, opts: Options) {
    println!("Advent Of Code");
    println!("{}", opts.usage(&format!("Usage: {} [options] year day input", program)));
}

// Boilerplate launch code that reads options and dispatches to specific year/day code
fn main() {
    let args: Vec<String> = env::args().map(|x| x.to_string())
                                       .collect();
    let mut opts = Options::new();
    opts.optopt("f", "file", "Filename to load input from", "NAME");

    // Parse command line
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(_) => { print_usage(&args[0], opts);
                    return; }
    };

    if matches.free.len() < 2 as usize {
        print_usage(&args[0], opts);
        return;
    }

    let year = &matches.free[0];
    let day = &matches.free[1];

    // Look for input
    // 1. On command line directly
    // 2. -f option
    // 3. Default filename input-{year}-{day}.txt
    let input = match matches.free.len() {
        3 => String::from(&matches.free[2]),
        _ => match matches.opt_str("f") {
                Some(f) => match fs::read_to_string(&f) {
                    Ok(o) => o,
                    Err(e) => { println!("Error reading file '{}':", f);
                                println!("\t{}\n", e);
                                print_usage(&args[0], opts);
                                return;},
                },
                None => match fs::read_to_string(format!("input-{}-{}.txt", year, day)) {
                    Ok(o) => o,
                    Err(e) => { println!("No input or filename specified and could not read default file '{}':", format!("input-{}-{}.txt", year, day));
                                println!("\t{}\n", e);
                                print_usage(&args[0], opts);
                                return;}
                },
        },
    };

    match &year[..] {
        "2015" => advent_2015::dispatch(day, &input),
        _ => println!("Unrecognized year: {}", year)
    }
}
